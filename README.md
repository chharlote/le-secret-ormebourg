# Le secret Ormebourg

Jeu textuel à choix créé en langage C en binome.

## Teaser

Dans l'ombre d'Ormebourg, village autrefois serein, les murmures sordides s'étirent comme des ombres malveillantes. Des lueurs inquiétantes et des silhouettes énigmatiques hantent la forêt, plongeant les rues pavées dans une anxiété grandissante. Surgi dans le village, un mage mystérieux, vêtu d'une robe étincelante, se dressant pour démêler les fils du mystère.

Toi, mag, portes le fardeau du destin d'Ormebourg. Sauras-tu être la lueur dissipant les mystères, ou l'ombre les engloutissant ? Entre les ruelles désertes et les bois antiques, chaque choix sculptera le récit de ce village plongé dans l'incertitude.

Au cœur de la forêt ancienne, la magie attend son élu. Seras-tu la lumière chassant les ténèbres et ramenant la joie à Ormebourg ? Le sort des habitants repose entre tes mains.

## Usage

Pour lancer le jeu il faut dans un terminal dans le dossier du jeu mettre : ./a.exe

