#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <windows.h>
#include <string.h>
/*---GERME CHARLOTTE ET CHATELAIN LILOU TPB*/


/*-----------------color-code-------------
-pink : \x1b[38;5;206m
-green : \x1b[32m
-blue : \x1b[34m
-yellow : \x1b[33m
-purple : \x1b[35m
-red : \x1b[31m
-orange : \x1b[38;5;208m
*/

#include <stdbool.h>
#include <time.h>

/*to clear the terminal*/
#include <conio.h>
#define CLEAR_SCREEN() system("cls")

/*-----Used fot labyrinth----------*/
#define SIZE_ROWS 10
#define SIZE_COLS 10

/*--Used in "display_colors"' procedure*/
#define max_word 7
#define max_word_lenth 20

/*--Used fo a battle--*/
typedef struct
{
    char name[20];
    int health_points;
} player;

/*--allows these sub-programs to be called from anywhere--*/
void choice_castle_kiosq(char *language, int *progress);

void castle_inside_1(char *language, int *progress);
void castle_inside_2(char *language, int *progress);
void castle_inside_3(char *language, int *progress);
void castle_inside_4(char *language, int *progress);

void dilemma_lever(char *language, int *progress);
void dilemma_lever_finish(char *language, int *progress);

void kiosq(char *language, int *progress);
void kiosq_inside(char *language, int *progress);

void choice_tunnel(char *language, int *progress);

int main();

char player_name[21];

/*--use to display text in files--*/
/*--the language parameter is used poru choose whether to open the English or French file--*/
/*--start_line and end_line to know which lines of the file to read--*/
/*--"color" allows to know which color the text will display--*/
void reading_text(char *language, int start_line, int end_line, char *color)
{

    char *text_en = "text_eng.txt";
    char *text_fr = "text_fr.txt";
    char line[2000];
    int number_line_en;
    number_line_en = 1;

    if (strcmp(language, "en") == 0)
    {
        FILE *text_file_en = fopen(text_en, "r");

        while (fgets(line, sizeof(line), text_file_en) != NULL)
        {
            if (start_line <= number_line_en && number_line_en <= end_line)
            {
                printf("%s%s\x1b[0m", color, line);
                Sleep(1);
            }
            number_line_en = number_line_en + 1;

            if (number_line_en > end_line)
            {
                break;
            }
        }
        fclose(text_file_en);
    }

    if (strcmp(language, "fr") == 0)
    {
        FILE *text_file_fr = fopen(text_fr, "r");

        while (fgets(line, sizeof(line), text_file_fr) != NULL)
        {
            if (number_line_en >= start_line && number_line_en <= end_line)
            {
                printf("%s%s\x1b[0m", color, line);
                Sleep(1);
            }
            number_line_en = number_line_en + 1;

            if (number_line_en > end_line)
            {
                break;
            }
        }
        fclose(text_file_fr);
    }
    printf("\n");
}

/*--text when the player chooses to leave the game--*/
/*--it is also the end of the program-*/
void end_game(char *language)
{
    reading_text(language, 75, 75, "\x1b[37m");
    exit(EXIT_SUCCESS);
}

/*--retrieving choices that are integers, the function checks
if the value entered by the player is an integer otherwise it empties the buffer--*/
int safe_input_read()
{
    int choice;
    int read = scanf("%d", &choice);

    if (read == 0)
    {
        while (getchar() != '\n')
            ; // Vidage du tampon d'entrée en cas d'échec de la lecture
    }
    return (choice);
}

/*------text if a wrong choice is made-------*/
void error_choice_2(char *language)
{
    reading_text(language, 39, 39, "\x1b[37m");
}

/*--The player has the option at certain times to restart the game or not--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void restart_game_inlife(char *language, int *progress)
{
    int choice;

    Sleep(3000);

    do
    {
        reading_text(language, 557, 559, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:

            main();
            break;
        case 2:

            end_game(language);

        default:
            error_choice_2(language);
            break;
        }
    } while (choice != 1 || choice != 2);
}

/*--displays the text when the player dies, he has the option to restart the game or not-- */
/*--There is also the choice to retake the game at the chackpoint when it has passed--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void restart_game_death(char *language, int *progress)
{
    int choice;

    reading_text(language, 700, 707, "\x1b[37m");
    Sleep(3000);

    if (*progress < 4)
    {
        do
        {
            reading_text(language, 557, 559, "\x1b[37m");
            choice = safe_input_read();

            switch (choice)
            {
            case 1:

                main();
                break;
            case 2:

                end_game(language);
                break;
            default:
                error_choice_2(language);
                break;
            }
        } while (choice != 1 || choice != 2);
    }
    else
    {
        do
        {
            reading_text(language, 431, 434, "\x1b[37m");
            scanf("%d", &choice);

            switch (choice)
            {
            case 1:

                kiosq_inside(language, progress);
                break;
            case 2:

                main();
                break;

            case 3:
                end_game(language);

            default:
                error_choice_2(language);
                break;
            }
        } while (choice != 1 || choice != 2);
    }
    /***/
    if (*progress >= 4)
    {
        do
        {
            reading_text(language, 431, 434, "\x1b[37m");
            scanf("%d", &choice);

            switch (choice)
            {
            case 1:

                kiosq_inside(language, progress);
                break;
            case 2:

                main();
                break;

            case 3:
                end_game(language);

            default:
                error_choice_2(language);
                break;
            }
        } while (choice != 1 || choice != 2);
    }
    else
    {
        do
        {
            reading_text(language, 557, 559, "\x1b[37m");
            scanf("%d", &choice);

            switch (choice)
            {
            case 1:

                main();
                break;
            case 2:

                end_game(language);
                break;
            default:
                error_choice_2(language);
                break;
            }
        } while (choice != 1 || choice != 2);
    }
}

/*-----text for language choice----*/
/*-----language is initialized----*/
void language_choice(char *language)
{
    int start_line, end_line, start_line_2, end_line_2;
    start_line = 1;
    end_line = 1;
    start_line_2 = 2;
    end_line_2 = 2;

    char *text_en = "text_eng.txt";
    char *text_fr = "text_fr.txt";
    char line[2000];
    int number_line_en;
    number_line_en = 1;

    do
    {
        FILE *text_file_fr = fopen(text_fr, "r");

        while (fgets(line, sizeof(line), text_file_fr) != NULL)
        {
            if (number_line_en >= start_line && number_line_en <= end_line)
            {
                printf("%s", line);
            }
            number_line_en = number_line_en + 1;

            if (number_line_en > end_line)
            {
                break;
            }
        }
        fclose(text_file_fr);
        scanf("%s", language);

        if (!(strcmp(language, "en") == 0 || strcmp(language, "fr") == 0))
        {
            printf("Bad language choice");
        }
    } while (!(strcmp(language, "en") == 0 || strcmp(language, "fr") == 0));

    reading_text(language, start_line_2, end_line_2, "\x1b[37m");
    Sleep(1500);
    CLEAR_SCREEN();
}

/*--Display the spell when the player needs it--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void spell_book(char *language, int *progress)
{
    if (*progress <= 2)
    {
        reading_text(language, 818, 833, "\x1b[37m");
    }
    else
    {
        reading_text(language, 800, 815, "\x1b[37m");
    }
}

/*---It's the end of the story with a last choice--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void story_end(char *language, int *progress)
{
    int choice;

    do
    {
        reading_text(language, 400, 402, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            reading_text(language, 404, 415, "\x1b[37m");
            restart_game_inlife(language, progress);

            break;

        case 2:

            reading_text(language, 417, 429, "\x1b[37m");
            restart_game_inlife(language, progress);

            break;

        default:
            error_choice_2(language);
            break;
        }
    } while (choice != 1 || choice != 2);
}

/*--displays the progress of the fight--*/
/*--the target and attacker parameter is used to display information based on the tower--*/
/*--damage updates the number of players' health--*/
/*--"language" is useful to know in which language to display the information-- */
void attack(player *attacker, player *target, int damage, char *language)
{
    if (strcmp(language, "fr") == 0)
    {
        printf("\n%s attaque %s et inflige %d points de dégâts!\n", attacker->name, target->name, damage);
        target->health_points = target->health_points - damage;
        printf("\n%s a maintenant %d health points.\n", target->name, target->health_points);
    }
    else
    {
        printf("\n%s attacks %s and inflicts %d points of damage !\n", attacker->name, target->name, damage);
        target->health_points = target->health_points - damage;
        printf("\n%s now has %d points de vie.\n", target->name, target->health_points);
    }
}

/*---- this is the number of health points that the sorcerer can lose to the player, it's random----*/
/*---- The procedure attack is called---*/
/*--setting "bot" and "target" are used to save the health points of the sorcerer and the player--*/
void sorcerer_attack(player *bot, player *cible, char *language)
{
    int damage;
    damage = rand() % 16;
    attack(bot, cible, damage, language);
}

/*---- It's a duel, the player have to kill the sorcerer, he can choose between different spell----*/
/*----each spell causes a different number of health points to be lost, and those health points lost are random--*/
/*---- The procedure spell_book, sorcerer_attack and attack are called or restart_game-death*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void sorcerer_battle(char *language, int *progress)
{

    int damage, choice, c;
    char spell[20];

    damage = 0;

    player real_player;
    strcpy(real_player.name, player_name);
    real_player.health_points = 100;
    player sorcerer = {"Sorcier", 100};

    srand(time(NULL));

    reading_text(language, 691, 691, "\x1b[37m");

    while (real_player.health_points > 0 && sorcerer.health_points > 0)
    {

        printf("%d ", real_player.health_points);
        reading_text(language, 692, 692, "\x1b[37m");

        printf("%d", sorcerer.health_points);
        reading_text(language, 693, 693, "\x1b[37m");

        printf("%s, ", real_player.name);
        reading_text(language, 694, 694, "\x1b[37m");

        do
        {
            reading_text(language, 695, 695, "\x1b[37m");

            choice = safe_input_read();

            if (choice == 1)
            {
                spell_book(language, progress);
            }
            else if (choice != 1 && choice != 2)
            {
                error_choice_2(language);
            }

        } while (choice != 1 && choice != 2);

        reading_text(language, 696, 696, "\x1b[37m");

        while ((c = getchar()) != '\n' && c != EOF)
            ;
        fgets(spell, sizeof(spell), stdin);
        spell[strcspn(spell, "\n")] = '\0';

        if (strcasecmp(spell, "lux radiance") == 0)
        {
            damage = rand() % 1;
            attack(&real_player, &sorcerer, damage, language);
        }
        else if (strcasecmp(spell, "fractus portae") == 0)
        {
            damage = rand() % 6;
            attack(&real_player, &sorcerer, damage, language);
        }
        else if (strcasecmp(spell, "invulnera gladii") == 0)
        {
            damage = rand() % 26;
            attack(&real_player, &sorcerer, damage, language);
        }
        else if (strcasecmp(spell, "glacius aurora") == 0)
        {
            damage = rand() % 21;
            attack(&real_player, &sorcerer, damage, language);
        }
        else if (strcasecmp(spell, "vis barriera") == 0)
        {
            damage = rand() % 1;
            attack(&real_player, &sorcerer, damage, language);
        }
        else
        {
            reading_text(language, 238, 238, "\x1b[37m");
        }

        sorcerer_attack(&sorcerer, &real_player, language);
    }

    if (real_player.health_points <= 0)
    {
        reading_text(language, 341, 341, "\x1b[37m");
        restart_game_death(language, progress);
    }
    else
    {
        reading_text(language, 343, 344, "\x1b[37m");
        story_end(language, progress);
    }
}

/*---- The player have to accept the duel ----*/
/*---- at the end, the procedure sorcerer_battle is called or restart_game-death*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void sorcerer_duel(char *language, int *progress)
{
    int choice, choice_2, choice_3, alternative;
    char spell[20];
    alternative = 0;

    reading_text(language, 316, 321, "\x1b[37m");

    do
    {
        reading_text(language, 323, 325, "\x1b[37m");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:

            if (alternative == 0)
            {
                reading_text(language, 333, 333, "\x1b[37m");
                sorcerer_battle(language, progress);
            }
            else
            {
                reading_text(language, 335, 335, "\x1b[37m");
                sorcerer_battle(language, progress);
            }

            break;

        case 2:
            if (alternative == 0)
            {
                reading_text(language, 327, 327, "\x1b[37m");
            }
            else if (alternative == 1)
            {
                reading_text(language, 329, 329, "\x1b[37m");
            }
            else
            {
                reading_text(language, 331, 331, "\x1b[37m");
                restart_game_death(language, progress);
            }

            alternative = alternative + 1;

            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1);
}

/*-----the procedure where the player had to fight against the monster to gain the key*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void fight_ball(char *language, int *progress)
{
    char spell[20];
    int fail = 0;
    int c;

    reading_text(language, 679, 679, "\x1b[37m");
    spell_book(language, progress);
    while ((c = getchar()) != '\n' && c != EOF)
        ;
    fgets(spell, sizeof(spell), stdin);
    spell[strcspn(spell, "\n")] = '\0';

    while (strcasecmp(spell, "glacius aurora") != 0)
    {

        reading_text(language, 683, 683, "\x1b[37m");
        fail++;

        if (fail > 3)
        {
            reading_text(language, 684, 684, "\x1b[37m");
            restart_game_death(language, progress);
        }

        spell_book(language, progress);
        scanf("%s", spell);
    }

    reading_text(language, 680, 681, "\x1b[37m");
    reading_text(language, 688, 690, "\x1b[37m");
    *progress = 6;
}

/*---- when the player goes into the left tunnel, he had to play the "magic square" ----*/
/*---- at the end of the test, the procedure fight_ball is called, to make the player fight against the little monster*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void left_tunnel(char *language, int *progress)
{
    int choice;

    if (*progress != 6)
    {
        reading_text(language, 560, 561, "\x1b[37m");
        reading_text(language, 565, 577, "\x1b[37m");
        scanf("%d", &choice);

        do
        {

            if (choice == 12)
            {
                reading_text(language, 581, 593, "\x1b[37m");
                scanf("%d", &choice);

                if (choice == 8)
                {
                    reading_text(language, 597, 609, "\x1b[37m");
                    scanf("%d", &choice);

                    if (choice == 11)
                    {
                        reading_text(language, 613, 625, "\x1b[37m");
                        scanf("%d", &choice);

                        if (choice == 16)
                        {
                            reading_text(language, 629, 641, "\x1b[37m");
                            scanf("%d", &choice);

                            if (choice == 4)
                            {
                                reading_text(language, 645, 657, "\x1b[37m");
                                scanf("%d", &choice);

                                if (choice == 14)
                                {
                                    reading_text(language, 661, 673, "\x1b[37m");
                                    reading_text(language, 678, 678, "\x1b[37m");
                                    fight_ball(language, progress);
                                }
                                else
                                {
                                    error_choice_2(language);
                                    reading_text(language, 565, 577, "\x1b[37m");
                                    scanf("%d", &choice);
                                }
                            }
                            else
                            {
                                error_choice_2(language);
                                reading_text(language, 565, 577, "\x1b[37m");
                                scanf("%d", &choice);
                            }
                        }
                        else
                        {
                            error_choice_2(language);
                            reading_text(language, 565, 577, "\x1b[37m");
                            scanf("%d", &choice);
                        }
                    }
                    else
                    {
                        error_choice_2(language);
                        reading_text(language, 565, 577, "\x1b[37m");
                        scanf("%d", &choice);
                    }
                }
                else
                {
                    error_choice_2(language);
                    reading_text(language, 565, 577, "\x1b[37m");
                    scanf("%d", &choice);
                }
            }
            else
            {
                error_choice_2(language);
                reading_text(language, 565, 577, "\x1b[37m");
            }

        } while (choice != 14);
    }

    else
    {
        reading_text(language, 686, 686, "\x1b[37m");
    }
}

/*--displays the labyrinth--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void display_laby(char labyrinth[SIZE_ROWS][SIZE_COLS])
{
    for (int i = 0; i < SIZE_ROWS; i++)
    {
        for (int j = 0; j < SIZE_COLS; j++)
        {
            printf("%c ", labyrinth[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

/*--recovers the movements of the player allowing his movement--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void player_laby_direction(char *language, char labyrinth[SIZE_ROWS][SIZE_COLS], int *player_row, int *player_col)
{

    int new_row = *player_row;
    int new_col = *player_col;
    int first_direction = getch();

    if (first_direction == 224)
    {
        int direction = getch();

        switch (direction)
        {
        case 75: /*-Left arrow-*/
            if (new_col > 0 && labyrinth[new_row][new_col - 1] != '#')
            {
                new_col--;
            }
            break;
        case 77: /*-Right arrow-*/
            if (new_col < SIZE_COLS - 1 && labyrinth[new_row][new_col + 1] != '#')
            {
                new_col++;
            }
            break;
        case 72: /*-Up arrow-*/
            if (new_row > 0 && labyrinth[new_row - 1][new_col] != '#')
            {
                new_row--;
            }
            break;
        case 80: /*-Down arrow-*/
            if (new_row < SIZE_ROWS - 1 && labyrinth[new_row + 1][new_col] != '#')
            {
                new_row++;
            }
            break;
        default:
            error_choice_2(language);
            Sleep(800);
            return;
        }

        /*-- Updates the player’s position in the labyrinth--*/
        labyrinth[*player_row][*player_col] = '.';
        labyrinth[new_row][new_col] = 'P';

        *player_row = new_row;
        *player_col = new_col;
    }
}

/*--initial position of the player and the game persists until the player reaches the exit.--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void mushroom_labyrinth(char *language)
{

    char labyrinth[SIZE_ROWS][SIZE_COLS] = {
        {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
        {'P', '.', '.', '.', '.', '#', '#', '.', '#', '#'},
        {'#', '.', '#', '.', '.', '.', '.', '.', '#', '#'},
        {'#', '.', '#', '.', '#', '#', '.', '#', '#', '#'},
        {'#', '.', '.', '.', '#', '.', '#', '.', '.', '#'},
        {'#', '#', '#', '.', '.', '.', '.', '.', '#', '#'},
        {'#', '.', '#', '.', '#', '.', '#', '.', '.', '#'},
        {'#', '.', '.', '.', '#', '.', '#', '#', '.', 'E'},
        {'#', '#', '#', '#', '#', '#', '#', '#', '#', '#'},
    };

    /*--Initialization of the player’s position at the entrance of the labyrinth--*/
    int player_row = 1;
    int player_col = 0;

    display_laby(labyrinth);
    reading_text(language, 710, 710, "\x1b[37m");

    while (player_row != 8 && player_col != 9)
    {
        reading_text(language, 711, 711, "\x1b[37m");

        player_laby_direction(language, labyrinth, &player_row, &player_col);

        system("cls");

        display_laby(labyrinth);
    }
}

/*--the player at the choice to make the labyrinth or not, at the end of the labyrinth there is a large door. */
/*--The player can open the door according to his advance in the game--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void right_tunnel(char *language, int *progress)
{

    int choice, choice_2, choice_3;
    int line_1, start_line_2, end_line_2, start_line_3, end_line_3, line_4;
    int start_line_5, end_line_5, start_line_6, end_line_6, end_line_6_2, line_7;
    char spell[20];

    line_1 = 297;

    start_line_2 = 298;
    end_line_2 = 300;

    start_line_3 = 302;
    end_line_3 = 304;

    line_4 = 306;

    start_line_5 = 308;
    end_line_5 = 309;

    start_line_6 = 310;
    end_line_6 = 312;
    end_line_6_2 = 313;

    line_7 = 314;

    reading_text(language, line_1, line_1, "\x1b[37m");

    do
    {
        reading_text(language, start_line_2, end_line_2, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            choice_tunnel(language, progress);
            break;

        case 2:

            mushroom_labyrinth(language);

            if (*progress != 6)
            {
                reading_text(language, start_line_5, end_line_5, "\x1b[37m");

                do
                {
                    reading_text(language, start_line_6, end_line_6, "\x1b[37m");
                    choice_3 = safe_input_read();

                    switch (choice_3)
                    {
                    case 1:
                        spell_book(language, progress);
                        scanf("%s", spell);
                        reading_text(language, line_7, line_7, "\x1b[37m");
                        choice_tunnel(language, progress);

                        break;

                    case 2:

                        choice_tunnel(language, progress);

                        break;

                    default:

                        error_choice_2(language);
                        break;
                    }

                } while (choice_3 != 1 || choice_3 != 2);
            }

            else if (*progress == 6)
            {

                reading_text(language, start_line_5, end_line_5, "\x1b[37m");

                do
                {
                    reading_text(language, start_line_6, end_line_6_2, "\x1b[37m");
                    choice_3 = safe_input_read();

                    switch (choice_3)
                    {
                    case 1:
                        spell_book(language, progress);
                        scanf("%s", spell);
                        reading_text(language, line_7, line_7, "\x1b[37m");
                        choice_tunnel(language, progress);

                        break;

                    case 2:

                        choice_tunnel(language, progress);

                        break;

                    case 3:

                        sorcerer_duel(language, progress);

                        break;

                    default:

                        error_choice_2(language);
                        break;
                    }

                } while (choice_2 != 1 || choice_2 != 2);
            }

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*----Choice of 2 events-----*/
/*---- The procedures right_tunnel and left_tunnel are called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void choice_tunnel(char *language, int *progress)
{

    int choice;
    char spell[20];

    do
    {
        reading_text(language, 285, 287, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            right_tunnel(language, progress);

            break;

        case 2:
            left_tunnel(language, progress);
            break;

        case 3:
            spell_book(language, progress);
            scanf("%s", spell);
            reading_text(language, 295, 295, "\x1b[37m");
            break;

        default:

            error_choice_2(language);
            break;
        }
    } while (choice != 1 || choice != 2);
}

/*---- The procedures spell_book is called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void cellars_castle(char *language, int *progress)
{
    int choice;
    char spell[20];

    reading_text(language, 279, 282, "\x1b[37m");
    reading_text(language, 284, 284, "\x1b[37m");

    do
    {
        reading_text(language, 285, 287, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:

            reading_text(language, 289, 289, "\x1b[37m");

            break;

        case 2:

            reading_text(language, 289, 289, "\x1b[37m");

            break;
        case 3:

            do
            {

                spell_book(language, progress);
                int c;
                while ((c = getchar()) != '\n' && c != EOF)
                    ;
                fgets(spell, sizeof(spell), stdin);
                spell[strcspn(spell, "\n")] = '\0';

                if (strcasecmp(spell, "lux radiance") == 0)
                {
                    reading_text(language, 293, 293, "\x1b[37m");
                    *progress = 5;
                    choice_tunnel(language, progress);
                }
                else
                {
                    reading_text(language, 291, 291, "\x1b[37m");
                }
            } while (!(strcasecmp(spell, "lux radiance") == 0));

            break;

        default:
            error_choice_2;
            break;
        }
    } while (choice != 3);
}

/*---- The procedures spell_book and cellars_castle are called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void kiosq_inside(char *language, int *progress)
{
    int choice, choice_2, choice_3, choice_4;
    char spell[20];

    do
    {
        reading_text(language, 246, 249, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:

            reading_text(language, 251, 251, "\x1b[37m");

            do
            {
                reading_text(language, 253, 257, "\x1b[37m");
                choice_2 = safe_input_read();

                switch (choice_2)
                {
                case 1:
                    reading_text(language, 265, 265, "\x1b[37m");

                    do
                    {

                        reading_text(language, 266, 269, "\x1b[37m");
                        choice_3 = safe_input_read();

                        switch (choice_3)
                        {
                        case 1:

                            reading_text(language, 271, 271, "\x1b[37m");

                            do
                            {
                                reading_text(language, 272, 275, "\x1b[37m");
                                choice_4 = safe_input_read();

                                switch (choice_4)
                                {
                                case 1:
                                    kiosq_inside(language, progress);
                                    break;

                                case 2:
                                    do
                                    {

                                        spell_book(language, progress);
                                        int c;
                                        while ((c = getchar()) != '\n' && c != EOF)
                                            ;
                                        fgets(spell, sizeof(spell), stdin);
                                        spell[strcspn(spell, "\n")] = '\0';

                                        if (strcasecmp(spell, "fractus portae") == 0)
                                        {
                                            reading_text(language, 277, 277, "\x1b[37m");
                                            *progress = 4;
                                            cellars_castle(language, progress);
                                        }
                                        else
                                        {
                                            reading_text(language, 239, 239, "\x1b[37m");
                                        }
                                    } while (!(strcasecmp(spell, "fractus portae") == 0));

                                    break;

                                default:
                                    error_choice_2(language);
                                    break;
                                }
                            } while (choice_4 != 1 || choice_4 != 4);

                            break;

                        case 2:

                            kiosq_inside(language, progress);

                            break;

                        default:
                            error_choice_2(language);
                            break;
                        }

                    } while (choice_3 != 1 || choice_3 != 2);

                    break;
                case 2:
                    reading_text(language, 259, 263, "\x1b[37m");
                    kiosq_inside(language, progress);
                    break;
                case 3:
                    kiosq_inside(language, progress);
                    break;

                default:
                    error_choice_2(language);
                    break;
                }

            } while (choice_2 != 1 || choice_2 != 2 || choice_2 != 3);

            break;
        case 2:
            reading_text(language, 259, 263, "\x1b[37m");
            kiosq_inside(language, progress);

            break;

        default:
            error_choice_2(language);
            break;
        }
    } while (choice != 1 || choice != 2);
}

/*----Choice of 4 events-----*/
/*---- The procedures dilemma_lever_finish, choice_castle_kiosq or restart_game_death are called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void castle_inside_4(char *language, int *progress)
{
    int choice;

    do
    {

        reading_text(language, 94, 99, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            dilemma_lever_finish(language, progress);
            break;
        case 2:
            reading_text(language, 68, 68, "\x1b[37m");
            restart_game_death(language, progress);
            break;

        case 3:
            reading_text(language, 126, 126, "\x1b[37m");
            castle_inside_4(language, progress);
            break;
        case 4:
            choice_castle_kiosq(language, progress);
            break;
        case 5:
            spell_book(language, progress);
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2 || choice != 3 || choice != 4);
}

/*----Quizz with 5 questions-----*/
/*----The procedures castle_inside_3, castle_inside_4 or restart_game_death(if 1 wrong answer) are called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void quizz_library_no_tea(char *language, int *progress)
{
    int answer_1, answer_2, answer_3, answer_4, answer_5;

    reading_text(language, 115, 115, "\x1b[37m");

    do
    {
        reading_text(language, 118, 122, "\x1b[37m");
        answer_1 = safe_input_read();

        switch (answer_1)
        {
        case 1:
            reading_text(language, 125, 125, "\x1b[37m");
            restart_game_death(language, progress);

            break;
        case 2:
            reading_text(language, 124, 124, "\x1b[37m");

            do
            {
                reading_text(language, 127, 131, "\x1b[37m");
                answer_2 = safe_input_read();

                switch (answer_2)
                {
                case 1:
                    reading_text(language, 125, 125, "\x1b[37m");
                    restart_game_death(language, progress);
                    break;

                case 2:
                    reading_text(language, 125, 125, "\x1b[37m");
                    restart_game_death(language, progress);
                    break;

                case 3:

                    reading_text(language, 124, 124, "\x1b[37m");
                    do
                    {
                        reading_text(language, 133, 137, "\x1b[37m");
                        answer_3 = safe_input_read();

                        switch (answer_3)
                        {
                        case 1:
                            reading_text(language, 125, 125, "\x1b[37m");
                            restart_game_death(language, progress);
                            break;

                        case 2:
                            reading_text(language, 124, 124, "\x1b[37m");
                            do
                            {
                                reading_text(language, 139, 143, "\x1b[37m");
                                answer_4 = safe_input_read();

                                switch (answer_4)
                                {
                                case 1:

                                    reading_text(language, 125, 125, "\x1b[37m");
                                    restart_game_death(language, progress);

                                    break;

                                case 2:

                                    reading_text(language, 125, 125, "\x1b[37m");
                                    restart_game_death(language, progress);

                                    break;

                                case 3:

                                    reading_text(language, 124, 124, "\x1b[37m");
                                    do
                                    {
                                        reading_text(language, 145, 149, "\x1b[37m");
                                        answer_5 = safe_input_read();

                                        switch (answer_5)
                                        {
                                        case 1:

                                            reading_text(language, 125, 125, "\x1b[37m");
                                            restart_game_death(language, progress);

                                            break;

                                        case 2:

                                            reading_text(language, 124, 124, "\x1b[37m");
                                            reading_text(language, 117, 117, "\x1b[37m");
                                            *progress = 3;
                                            castle_inside_4(language, progress);

                                            break;

                                        case 3:

                                            reading_text(language, 125, 125, "\x1b[37m");
                                            restart_game_death(language, progress);

                                            break;

                                        default:
                                            error_choice_2(language);
                                            break;
                                        }

                                    } while (answer_5 != 1 || answer_5 != 2 || answer_5 != 3);
                                    break;

                                default:

                                    error_choice_2(language);
                                    break;
                                }

                            } while (answer_4 != 1 || answer_4 != 2 || answer_4 != 3);

                            break;

                        case 3:
                            reading_text(language, 125, 125, "\x1b[37m");
                            restart_game_death(language, progress);
                            break;

                        default:
                            error_choice_2(language);
                            break;
                        }

                    } while (answer_3 != 1 || answer_3 != 2 || answer_3 != 3);

                    break;

                default:
                    error_choice_2(language);
                    break;
                }

            } while (answer_2 != 1 || answer_2 != 2 || answer_2 != 3);

            break;
        case 3:
            reading_text(language, 125, 125, "\x1b[37m");
            restart_game_death(language, progress);
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (answer_1 != 1 || answer_1 != 2 || answer_1 != 3);
}

/*----Quizz with 5 questions-----*/
/*----The procedures castle_inside_3, castle_inside_4 or restart_game_death (if 2 wrong answers) are called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void quizz_library_tea(char *language, int *progress)
{
    int answer_1, answer_2, answer_3, answer_4, answer_5;
    int progress_quizz;
    progress_quizz = 0;

    reading_text(language, 115, 115, "\x1b[37m");
    reading_text(language, 116, 116, "\x1b[37m");

    do
    {
        reading_text(language, 118, 122, "\x1b[37m");
        answer_1 = safe_input_read();

        switch (answer_1)
        {
        case 1:

            progress_quizz = progress_quizz + 1;

            if (progress_quizz == 1)
            {
                reading_text(language, 123, 123, "\x1b[37m");
            }

            else if (progress_quizz == 2)
            {
                reading_text(language, 125, 125, "\x1b[37m");
                restart_game_death(language, progress);
            }

            break;
        case 2:
            reading_text(language, 124, 124, "\x1b[37m");

            do
            {
                reading_text(language, 127, 131, "\x1b[37m");
                answer_2 = safe_input_read();

                switch (answer_2)
                {
                case 1:
                    progress_quizz = progress_quizz + 1;

                    if (progress_quizz == 1)
                    {
                        reading_text(language, 123, 123, "\x1b[37m");
                    }

                    else if (progress_quizz == 2)
                    {
                        reading_text(language, 125, 125, "\x1b[37m");
                        restart_game_death(language, progress);
                    }
                    break;

                case 2:
                    progress_quizz = progress_quizz + 1;

                    if (progress_quizz == 1)
                    {
                        reading_text(language, 123, 123, "\x1b[37m");
                    }

                    else if (progress_quizz == 2)
                    {
                        reading_text(language, 125, 125, "\x1b[37m");
                        restart_game_death(language, progress);
                    }
                    break;

                case 3:

                    reading_text(language, 124, 124, "\x1b[37m");
                    do
                    {
                        reading_text(language, 133, 137, "\x1b[37m");
                        answer_3 = safe_input_read();

                        switch (answer_3)
                        {
                        case 1:
                            progress_quizz = progress_quizz + 1;

                            if (progress_quizz == 1)
                            {
                                reading_text(language, 123, 123, "\x1b[37m");
                            }

                            else if (progress_quizz == 2)
                            {
                                reading_text(language, 125, 125, "\x1b[37m");
                                restart_game_death(language, progress);
                            }
                            break;

                        case 2:
                            reading_text(language, 124, 124, "\x1b[37m");
                            do
                            {
                                reading_text(language, 139, 143, "\x1b[37m");
                                answer_4 = safe_input_read();

                                switch (answer_4)
                                {
                                case 1:

                                    progress_quizz = progress_quizz + 1;

                                    if (progress_quizz == 1)
                                    {
                                        reading_text(language, 123, 123, "\x1b[37m");
                                    }

                                    else if (progress_quizz == 2)
                                    {
                                        reading_text(language, 125, 125, "\x1b[37m");
                                        restart_game_death(language, progress);
                                    }

                                    break;

                                case 2:

                                    progress_quizz = progress_quizz + 1;

                                    if (progress_quizz == 1)
                                    {
                                        reading_text(language, 123, 123, "\x1b[37m");
                                    }

                                    else if (progress_quizz == 2)
                                    {
                                        reading_text(language, 125, 125, "\x1b[37m");
                                        restart_game_death(language, progress);
                                    }

                                    break;

                                case 3:

                                    reading_text(language, 124, 124, "\x1b[37m");
                                    do
                                    {
                                        reading_text(language, 145, 149, "\x1b[37m");
                                        answer_5 = safe_input_read();

                                        switch (answer_5)
                                        {
                                        case 1:

                                            progress_quizz = progress_quizz + 1;

                                            if (progress_quizz == 1)
                                            {
                                                reading_text(language, 123, 123, "\x1b[37m");
                                            }

                                            else if (progress_quizz == 2)
                                            {
                                                reading_text(language, 125, 125, "\x1b[37m");
                                                restart_game_death(language, progress);
                                            }

                                            break;

                                        case 2:

                                            reading_text(language, 124, 124, "\x1b[37m");
                                            reading_text(language, 117, 117, "\x1b[37m");
                                            *progress = 3;
                                            castle_inside_4(language, progress);

                                            break;

                                        case 3:

                                            progress_quizz = progress_quizz + 1;

                                            if (progress_quizz == 1)
                                            {
                                                reading_text(language, 123, 123, "\x1b[37m");
                                            }

                                            else if (progress_quizz == 2)
                                            {
                                                reading_text(language, 125, 125, "\x1b[37m");
                                                restart_game_death(language, progress);
                                            }

                                            break;

                                        default:
                                            error_choice_2(language);
                                            break;
                                        }

                                    } while (answer_5 != 1 || answer_5 != 2 || answer_5 != 3);
                                    break;

                                default:

                                    error_choice_2(language);
                                    break;
                                }

                            } while (answer_4 != 1 || answer_4 != 2 || answer_4 != 3);

                            break;

                        case 3:
                            progress_quizz = progress_quizz + 1;

                            if (progress_quizz == 1)
                            {
                                reading_text(language, 123, 123, "\x1b[37m");
                            }

                            else if (progress_quizz == 2)
                            {
                                reading_text(language, 125, 125, "\x1b[37m");
                                restart_game_death(language, progress);
                            }
                            break;

                        default:
                            error_choice_2(language);
                            break;
                        }

                    } while (answer_3 != 1 || answer_3 != 2 || answer_3 != 3);

                    break;

                default:
                    error_choice_2(language);
                    break;
                }

            } while (answer_2 != 1 || answer_2 != 2 || answer_2 != 3);

            break;
        case 3:

            progress_quizz = progress_quizz + 1;

            if (progress_quizz == 1)
            {
                reading_text(language, 123, 123, "\x1b[37m");
            }

            else if (progress_quizz == 2)
            {
                reading_text(language, 125, 125, "\x1b[37m");
                restart_game_death(language, progress);
            }
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (answer_1 != 2);
}

/*----2 possible choices-----*/
/*---- The procedures quizz_library_tea or quizz_library_no_tea are called based on choices ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void library(char *language, int *progress)
{
    int choice, choice_2;

    do
    {
        reading_text(language, 100, 102, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:

            reading_text(language, 104, 106, "\x1b[37m");

            do
            {
                reading_text(language, 107, 109, "\x1b[37m");
                choice_2 = safe_input_read();

                switch (choice_2)
                {
                case 1:
                    reading_text(language, 111, 111, "\x1b[37m");
                    quizz_library_tea(language, progress);

                    break;
                case 2:
                    reading_text(language, 113, 113, "\x1b[37m");
                    quizz_library_no_tea(language, progress);

                    break;

                default:

                    error_choice_2(language);
                    library(language, progress);
                    break;
                }

            } while (choice_2 != 1 || choice_2 != 2);

            break;

        case 2:

            castle_inside_3(language, progress);

            break;

        default:
            error_choice_2(language);
            break;
        }
    } while (choice != 1 || choice != 2);
}

/*----Choice of 4 events-----*/
/*---- The procedures dilemma_lever_finish, library, choice_castle_kiosq or restart_game_death are called after a message appears ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void castle_inside_3(char *language, int *progress)
{
    int choice;
    char spell[20];
    do
    {

        reading_text(language, 94, 99, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            CLEAR_SCREEN();
            dilemma_lever_finish(language, progress);
            break;
        case 2:
            CLEAR_SCREEN();
            reading_text(language, 68, 68, "\x1b[37m");
            restart_game_death(language, progress);
            break;

        case 3:
            CLEAR_SCREEN();
            library(language, progress);
            break;
        case 4:
            CLEAR_SCREEN();
            choice_castle_kiosq(language, progress);
            break;
        case 5:
            CLEAR_SCREEN();
            int c;
            while ((c = getchar()) != '\n' && c != EOF)
                ;

            spell_book(language, progress);
            fgets(spell, sizeof(spell), stdin);

            spell[strcspn(spell, "\n")] = '\0';

            if (strcasecmp(spell, "lux radiance") == 0)
            {
                reading_text(language, 223, 223, "\x1b[37m");
                Sleep(8000);
            }
            else if (strcasecmp(spell, "fractus portae") == 0)
            {
                reading_text(language, 226, 227, "\x1b[37m");
                Sleep(8000);
            }
            else if (strcasecmp(spell, "invulnera gladii") == 0)
            {
                reading_text(language, 229, 230, "\x1b[37m");
                Sleep(8000);
            }
            else if (strcasecmp(spell, "glacius aurora") == 0)
            {
                reading_text(language, 228, 228, "\x1b[37m");
                Sleep(8000);
            }
            else
            {
                reading_text(language, 238, 238, "\x1b[37m");
                Sleep(8000);
            }

            break;

            castle_inside_3(language, progress);
        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2 || choice != 3 || choice != 4);
}

/*---- The procedure castle_inside_3 is called after a message appears ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void dilemma_lever_finish(char *language, int *progress)
{
    int choice, repeat;
    char spell[50];
    repeat = 0;

    reading_text(language, 90, 91, "\x1b[37m");
    choice = safe_input_read();

    do
    {
        switch (choice)
        {
        case 1:

            int c;
            while ((c = getchar()) != '\n' && c != EOF)
                ;

            spell_book(language, progress);
            fgets(spell, sizeof(spell), stdin);

            spell[strcspn(spell, "\n")] = '\0';

            if (strcasecmp(spell, "lux radiance") == 0)
            {
                reading_text(language, 223, 223, "\x1b[37m");
            }
            else if (strcasecmp(spell, "fractus portae") == 0)
            {
                reading_text(language, 226, 227, "\x1b[37m");
            }
            else if (strcasecmp(spell, "invulnera gladii") == 0)
            {
                reading_text(language, 229, 230, "\x1b[37m");
            }
            else if (strcasecmp(spell, "glacius aurora") == 0)
            {
                reading_text(language, 228, 228, "\x1b[37m");
            }
            else
            {
                reading_text(language, 238, 238, "\x1b[37m");
            }

            if (repeat == 0)
            {
                reading_text(language, 92, 92, "\x1b[37m");
                repeat = 1;
            }
            else
            {
                reading_text(language, 93, 93, "\x1b[37m");
            }

            castle_inside_3(language, progress);
            break;
        case 2:
            if (repeat == 0)
            {
                reading_text(language, 92, 92, "\x1b[37m");
                repeat = 1;
            }
            else
            {
                reading_text(language, 93, 93, "\x1b[37m");
            }

            castle_inside_3(language, progress);

            break;

        default:
            error_choice_2(language);
            dilemma_lever_finish(language, progress);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*----Choice of 2 events-----*/
/*---- The procedures castle_inside or restart_game_death are called based on the choice ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void dilemma_lever(char *language, int *progress)
{
    int choice, choice_2, choice_3;

    do
    {
        CLEAR_SCREEN();
        reading_text(language, 903, 914, "\x1b[38;5;208m");
        reading_text(language, 70, 73, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            do
            {
                reading_text(language, 77, 80, "\x1b[37m");
                choice_2 = safe_input_read();

                switch (choice_2)
                {
                case 1:
                    reading_text(language, 82, 82, "\x1b[37m");
                    restart_game_death(language, progress);

                    break;

                case 2:

                    do
                    {
                        reading_text(language, 84, 85, "\x1b[37m");
                        reading_text(language, 86, 88, "\x1b[37m");

                        choice_3 = safe_input_read();

                        switch (choice_3)
                        {
                        case 1:
                            *progress = 2;
                            CLEAR_SCREEN();
                            dilemma_lever_finish(language, progress);
                            break;
                        case 2:
                            CLEAR_SCREEN();
                            reading_text(language, 92, 92, "\x1b[37m");
                            castle_inside_3(language, progress);
                            break;

                        default:
                            error_choice_2(language);
                            break;
                        }
                    } while (choice_3 != 1 || choice_3 != 2);

                    break;

                default:
                    error_choice_2(language);
                    break;
                }
            } while (choice_2 != 1 || choice_2 != 2);

            break;

        case 2:
            castle_inside_2(language, progress);

            break;
        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*---- The procedures dilemma_lever and restart_game_death are called based on the choice ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void castle_inside_2(char *language, int *progress)
{
    int choice;
    char spell[20];

    do
    {

        CLEAR_SCREEN();
        reading_text(language, 63, 67, "\x1b[37m");
        reading_text(language, 919, 930, "\x1b[38;5;208m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            dilemma_lever(language, progress);
            break;

        case 2:
            reading_text(language, 68, 68, "\x1b[37m");
            restart_game_death(language, progress);

            break;
        case 3:
            int c;
            while ((c = getchar()) != '\n' && c != EOF)
                ;

            spell_book(language, progress);
            fgets(spell, sizeof(spell), stdin);

            spell[strcspn(spell, "\n")] = '\0';

            if (strcasecmp(spell, "lux radiance") == 0)
            {
                reading_text(language, 223, 223, "\x1b[37m");
                Sleep(8000);
            }
            else if (strcasecmp(spell, "fractus portae") == 0)
            {
                reading_text(language, 226, 227, "\x1b[37m");
                Sleep(8000);
            }
            else if (strcasecmp(spell, "invulnera gladii") == 0)
            {
                reading_text(language, 229, 230, "\x1b[37m");
                Sleep(8000);
            }
            else if (strcasecmp(spell, "glacius aurora") == 0)
            {
                reading_text(language, 228, 228, "\x1b[37m");
                Sleep(8000);
            }
            else
            {
                reading_text(language, 238, 238, "\x1b[37m");
                Sleep(8000);
            }
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*-----------------------------Choice of 3 events--------------------------------*/

/*--when the player unter this room he had to play a hangman game
to gain the spell_book--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void hangman_game(char *language, int *progress)
{
    int nb_rand, i, choice;
    char *word_to_guess;

    char hidden_word[strlen(word_to_guess)];
    char guess;
    srand(time(NULL));
    nb_rand = rand() % 6;

    if (nb_rand == 1)
    {

        if (strcmp(language, "fr") == 0)
        {
            word_to_guess = "enchantement";
        }

        else
        {
            word_to_guess = "enchantment";
        }
    }

    if (nb_rand == 2)
    {

        if (strcmp(language, "fr") == 0)
        {
            word_to_guess = "sorcier";
        }

        else
        {
            word_to_guess = "Wizard";
        }
    }

    if (nb_rand == 3)
    {

        word_to_guess = "ormebourg";
    }

    if (nb_rand == 4)
    {

        if (strcmp(language, "fr") == 0)
        {
            word_to_guess = "chateau";
        }

        else
        {
            word_to_guess = "castle";
        }
    }

    if (nb_rand == 5)
    {
        word_to_guess = "incantation";
    }

    do
    {
        CLEAR_SCREEN();
        reading_text(language, 903, 914, "\x1b[38;5;208m");
        reading_text(language, 454, 454, "\x1b[37m");

        /*------allows to replace the word by "_" ------*/

        for (int i = 0; i < strlen(word_to_guess); i++)
        {
            hidden_word[i] = '_';
        }
        hidden_word[strlen(word_to_guess)] = '\0';

        int incorrectGuesses = 0;
        int maxIncorrectGuesses = 6;

        do
        {

            printf("Mot à deviner : %s\n", hidden_word);

            reading_text(language, 550, 550, "\x1b[37m");
            scanf(" %c", &guess);
            CLEAR_SCREEN();

            bool letter_found = false;
            for (int i = 0; i < strlen(word_to_guess); i++)
            {
                if (word_to_guess[i] == guess)
                {
                    hidden_word[i] = guess;
                    letter_found = true;
                }
            }

            if (letter_found == false)
            {

                incorrectGuesses++;
                printf("Lettre incorrecte. Erreurs : %d\n", incorrectGuesses);

                switch (incorrectGuesses)
                {

                case 1:
                    reading_text(language, 457, 457, "\x1b[37m");
                    break;

                case 2:
                    reading_text(language, 462, 474, "\x1b[37m");
                    break;

                case 3:
                    reading_text(language, 480, 494, "\x1b[37m");
                    break;

                case 4:
                    reading_text(language, 498, 512, "\x1b[37m");
                    break;

                case 5:
                    reading_text(language, 516, 530, "\x1b[37m");
                    break;

                case 6:
                    reading_text(language, 534, 548, "\x1b[37m");
                    break;

                default:
                    error_choice_2(language);
                    break;
                }

                printf("\n");
            }

        } while (incorrectGuesses < maxIncorrectGuesses && strcmp(hidden_word, word_to_guess) != 0);

        if (strcmp(hidden_word, word_to_guess) == 0)
        {
            reading_text(language, 553, 553, "\x1b[37m");
            printf("%s", word_to_guess);
            Sleep(1000);
            *progress = 1;
            reading_text(language, 221, 221, "\x1b[35m");
            Sleep(14000);
            castle_inside_2(language, progress);
        }
        else
        {
            reading_text(language, 555, 555, "\x1b[37m");
            printf("%s", word_to_guess);
            restart_game_death(language, progress);
        }

    } while (choice != 1 || choice != 2);
}

/*--useful for echos_room function--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void display_colors(char *language, int *progress)
{
    int line_blue, line_purple, line_pink,
        line_orange, line_yellow, line_red, line_green,
        counter_3, counter_2, counter_1, question_enig,
        test_solve, test_wrong, i, choice;

    char *correct_sequence_fr[] = {"rose", "bleu", "orange", "bleu", "vert", "rouge", "violet"};
    char user_sequence_fr[max_word][max_word_lenth]; // 7 chaines de caratères avec 5 caractères max

    counter_3 = 356;
    counter_2 = 357;
    counter_1 = 358;
    line_blue = 361;
    line_green = 362;
    line_orange = 364;
    line_pink = 367;
    line_purple = 366;
    line_yellow = 363;
    line_red = 365;
    question_enig = 369;
    test_solve = 221;
    test_wrong = 371;

    do
    {
        CLEAR_SCREEN();
        reading_text(language, counter_3, counter_3, "\x1b[37m");
        Sleep(1000);
        reading_text(language, counter_2, counter_2, "\x1b[37m");
        Sleep(1000);
        reading_text(language, counter_1, counter_1, "\x1b[37m");
        Sleep(1000);

        CLEAR_SCREEN();
        reading_text(language, line_pink, line_pink, "\x1b[34m");
        Sleep(1200);
        CLEAR_SCREEN();
        reading_text(language, line_blue, line_blue, "\x1b[31m");
        Sleep(1200);
        CLEAR_SCREEN();
        reading_text(language, line_orange, line_orange, "\x1b[32m");
        Sleep(1200);
        CLEAR_SCREEN();
        reading_text(language, line_blue, line_blue, "\x1b[33m");
        Sleep(1200);
        CLEAR_SCREEN();
        reading_text(language, line_green, line_green, "\x1b[38;5;206m");
        Sleep(1200);
        CLEAR_SCREEN();
        reading_text(language, line_red, line_red, "\x1b[35m");
        Sleep(1200);
        CLEAR_SCREEN();
        reading_text(language, line_purple, line_purple, "\x1b[38;5;208m");
        Sleep(1300);
        CLEAR_SCREEN();

        reading_text(language, question_enig, question_enig, "\x1b[37m");
        printf("\n");

        for (i = 0; i < max_word; i++)
        {
            scanf("%s", user_sequence_fr[i]);
        }

        int sequence_true = 1;

        for (i = 0; i < max_word; i++)
        {
            if (strcmp(user_sequence_fr[i], correct_sequence_fr[i]) != 0)
            {
                sequence_true = 0;
                break;
            }
        }

        if (sequence_true == 1)
        {
            reading_text(language, test_solve, test_solve, "\x1b[37m");
            Sleep(14000);
            *progress = 1;
            castle_inside_2(language, progress);
        }

        if (sequence_true == 0)
        {
            reading_text(language, test_wrong, test_wrong, "\x1b[37m");
            restart_game_death(language, progress);
        }

    } while (choice != 1 || choice != 2);
}

/*--when the player unter this room he had to play a memory game
to gain the spell_book--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void echos_room(char *language, int *progress)
{
    int i, choice;

    do
    {
        CLEAR_SCREEN();
        reading_text(language, 903, 914, "\x1b[38;5;208m");
        reading_text(language, 350, 350, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            reading_text(language, 351, 351, "\x1b[37m");
            Sleep(14000);
            reading_text(language, 355, 355, "\x1b[37m");
            choice = safe_input_read();

            switch (choice)
            {
            case 1:
                display_colors(language, progress);
                break;

            case 2:
                reading_text(language, 353, 353, "\x1b[37m");
                Sleep(1000);
                castle_inside_1(language, progress);
                break;

            default:
                error_choice_2(language);
                break;
            }

            break;

        case 2:
            castle_inside_1(language, progress);
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*--when the player unter this room he had to play a matematical game
to gain the spell_book--*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void passage_enchanted_figures(char *language, int *progress)
{
    int i, choice;
    do
    {
        CLEAR_SCREEN();
        reading_text(language, 903, 914, "\x1b[38;5;208m");
        reading_text(language, 150, 152, "\x1b[37m");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            reading_text(language, 155, 163, "\x1b[37m");
            scanf("%d", &choice);

            switch (choice)
            {
            case 2:
                reading_text(language, 167, 168, "\x1b[37m");
                restart_game_death(language, progress);
                break;

            case 0:
                reading_text(language, 167, 168, "\x1b[37m");
                restart_game_death(language, progress);
                break;

            case 1:
                reading_text(language, 171, 176, "\x1b[35m");
                scanf("%d", &choice);

                switch (choice)
                {

                case 5:
                    reading_text(language, 167, 168, "\x1b[37m");
                    restart_game_death(language, progress);
                    break;

                case 4:
                    reading_text(language, 167, 168, "\x1b[37m");
                    restart_game_death(language, progress);
                    break;

                case 3:
                    reading_text(language, 178, 186, "\x1b[35m");
                    scanf("%d", &choice);

                    switch (choice)
                    {

                    case 5:
                        reading_text(language, 167, 168, "\x1b[37m");
                        restart_game_death(language, progress);
                        break;

                    case 9:
                        reading_text(language, 167, 168, "\x1b[37m");
                        restart_game_death(language, progress);
                        break;

                    case 6:
                        reading_text(language, 186, 191, "\x1b[35m");
                        scanf("%d", &choice);

                        switch (choice)
                        {

                        case 13:
                            reading_text(language, 167, 168, "\x1b[37m");
                            restart_game_death(language, progress);
                            break;

                        case 12:
                            reading_text(language, 167, 168, "\x1b[37m");
                            restart_game_death(language, progress);
                            break;

                        case 10:
                            reading_text(language, 193, 198, "\x1b[35m");
                            scanf("%d", &choice);

                            switch (choice)
                            {

                            case 16:
                                reading_text(language, 167, 168, "\x1b[37m");
                                restart_game_death(language, progress);
                                break;

                            case 13:
                                reading_text(language, 167, 168, "\x1b[37m");
                                restart_game_death(language, progress);
                                break;

                            case 15:
                                reading_text(language, 200, 205, "\x1b[35m");
                                scanf("%d", &choice);

                                switch (choice)
                                {

                                case 19:
                                    reading_text(language, 167, 168, "\x1b[37m");
                                    restart_game_death(language, progress);
                                    break;

                                case 22:
                                    reading_text(language, 167, 168, "\x1b[37m");
                                    restart_game_death(language, progress);
                                    break;

                                case 21:
                                    reading_text(language, 207, 212, "\x1b[35m");
                                    scanf("%d", &choice);

                                    switch (choice)
                                    {

                                    case 29:
                                        reading_text(language, 167, 168, "\x1b[37m");
                                        restart_game_death(language, progress);
                                        break;

                                    case 25:
                                        reading_text(language, 167, 168, "\x1b[37m");
                                        restart_game_death(language, progress);
                                        break;

                                    case 28:
                                        reading_text(language, 214, 221, "\x1b[35m");
                                        *progress = 1;
                                        Sleep(20000);
                                        castle_inside_2(language, progress);
                                        break;

                                    default:
                                        restart_game_death(language, progress);
                                        break;
                                    }

                                    break;

                                default:
                                    reading_text(language, 167, 168, "\x1b[37m");
                                    restart_game_death(language, progress);
                                    break;
                                }

                                break;

                            default:
                                reading_text(language, 167, 168, "\x1b[37m");
                                restart_game_death(language, progress);
                                break;
                            }

                            break;

                        default:
                            reading_text(language, 167, 168, "\x1b[37m");
                            restart_game_death(language, progress);
                            break;
                        }

                        break;

                    default:
                        reading_text(language, 167, 168, "\x1b[37m");
                        restart_game_death(language, progress);
                        break;
                    }

                    break;

                default:
                    reading_text(language, 167, 168, "\x1b[37m");
                    restart_game_death(language, progress);
                    break;
                }

                break;

            default:
                reading_text(language, 167, 168, "\x1b[37m");
                restart_game_death(language, progress);
                break;
            }

            break;

        case 2:
            castle_inside_1(language, progress);
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*----Called when kiosk choice is made in choice_castle_kiosq.----*/
/*----There are 3 choices to make. At the end either it is the end of the game or we come back in choice_castle_kiosq---*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void kiosq(char *language, int *progress)
{
    int choice, choice_2, choice_3, c;

    do
    {
        CLEAR_SCREEN();
        reading_text(language, 42, 44, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            do
            {

                reading_text(language, 46, 49, "\x1b[37m");
                choice_2 = safe_input_read();

                switch (choice_2)
                {
                case 1:
                    if (*progress == 0 || *progress == 1 || *progress == 2)
                    {
                        reading_text(language, 51, 51, "\x1b[37m");
                        restart_game_death(language, progress);
                    }
                    else
                    {
                        char spell[20];
                        do
                        {
                            reading_text(language, 50, 50, "\x1b[37m");
                            spell_book(language, progress);
                            while ((c = getchar()) != '\n' && c != EOF)
                                ;
                            fgets(spell, sizeof(spell), stdin);
                            spell[strcspn(spell, "\n")] = '\0';

                            if (strcasecmp(spell, "vis barriera") == 0)
                            {
                                reading_text(language, 245, 245, "\x1b[37m");
                                Sleep(3000);
                                reading_text(language, 241, 244, "\x1b[37m");
                                kiosq_inside(language, progress);
                            }
                            else
                            {
                                reading_text(language, 239, 239, "\x1b[37m");
                            }
                        } while (!(strcasecmp(spell, "vis barriera") == 0));
                    }

                    break;
                case 2:

                    do
                    {
                        reading_text(language, 53, 55, "\x1b[37m");
                        choice_3 = safe_input_read();

                        switch (choice_3)
                        {
                        case 1:
                            choice_castle_kiosq(language, progress);
                            break;
                        case 2:
                            reading_text(language, 57, 57, "\x1b[37m");
                            choice_castle_kiosq(language, progress);
                            break;
                        default:
                            error_choice_2(language);
                            break;
                        }

                    } while (choice_3 != 1 || choice_3 != 2);

                    break;

                default:
                    error_choice_2(language);
                    kiosq(language, progress);
                    break;
                }

                break;
            } while (choice_2 != 1 || choice_2 != 2);

        case 2:
            choice_castle_kiosq(language, progress);
            break;

        default:
            error_choice_2(language);
            kiosq(language, progress);

            break;
        }
    } while (choice != 1 || choice != 2);
}

/*----Choice of 3 events-----*/
/*---- The procedures passage_enchanted_figures, echos_room, truc or choice_castle_kiosq are called based on the choice ----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void castle_inside_1(char *language, int *progress)
{
    int choice;
    CLEAR_SCREEN();
    reading_text(language, 32, 37, "\x1b[37m");
    reading_text(language, 890, 901, "\x1b[38;5;208m");
    choice = safe_input_read();

    do
    {
        switch (choice)
        {
        case 1:

            passage_enchanted_figures(language, progress);
            break;
        case 2:
            echos_room(language, progress);
            break;
        case 3:
            hangman_game(language, progress);
            break;
        case 4:
            choice_castle_kiosq(language, progress);
            break;

        default:
            error_choice_2(language);
            Sleep(1000);
            castle_inside_1(language, progress);
            break;
        }
    } while (choice != 1 || choice != 2 || choice != 3 || choice != 4);
}

/*----Choice between entering castle or kiosk----*/
/*---- The castle_inside_1 or kiosq functions are called based on the choice made-----*/
/*----error_choice_2 is called if a choice is not correct----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void choice_castle_kiosq(char *language, int *progress)
{
    int choice;

    do
    {
        reading_text(language, 28, 30, "\x1b[37m");
        reading_text(language, 840, 859, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            if (*progress == 0)
            {
                castle_inside_1(language, progress);
            }
            else if (*progress == 2)
            {
                castle_inside_3(language, progress);
            }
            else if (*progress == 3)
            {
                castle_inside_4(language, progress);
            }

            break;
        case 2:
            if (*progress <= 2)
            {
                kiosq(language, progress);
            }
            else if (*progress == 3)
            {
                kiosq(language, progress);
            }
            break;

        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 || choice != 2);
}

/*----Written early history castle---*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void castle_entrance(char *language, int *progress)
{
    CLEAR_SCREEN();
    reading_text(language, 17, 27, "\x1b[37m");
    choice_castle_kiosq(language, progress);
}

/*----choice of player name-----*/
/*-----player_name is initialized----*/
/*--setting "language" is used to call the reading_text procedure
and "player_name" to save the name of the player--*/
void name_choice(char *player_name, char *language)
{
    reading_text(language, 9, 9, "\x1b[37m");
    scanf("%s", player_name);

    CLEAR_SCREEN();

    printf("%s", player_name);
    reading_text(language, 10, 10, "\x1b[37m");
}

/*----Writing the beginning of the story----*/
/*----Choice finish game or continue----*/
/*--setting "language" is used to call the reading_text procedure
and "progress" to save the player's progress--*/
void debut(char *language, int *progress)
{
    int choice;

    reading_text(language, 12, 13, "\x1b[37m");

    do
    {
        reading_text(language, 14, 14, "\x1b[37m");
        reading_text(language, 865, 885, "\x1b[37m");
        choice = safe_input_read();

        switch (choice)
        {
        case 1:
            castle_entrance(language, progress);
            break;
        case 2:
            reading_text(language, 15, 15, "\x1b[37m");
            restart_game_inlife(language, progress);
            break;
        default:
            error_choice_2(language);
            break;
        }

    } while (choice != 1 && choice != 2);
}

/*----displays the introduction of the game----*/
/*--setting "language" is used to call the reading_text procedure*/
void story_text(char *language)
{
    reading_text(language, 3, 8, "\x1b[37m");
}

int main()
{
    /*allow to put the terminal in fullscreen*/
    HWND hwnd = GetForegroundWindow();
    ShowWindow(hwnd, SW_MAXIMIZE);
    CLEAR_SCREEN();

    char language[3];
    int progress;
    progress = 0;

    language_choice(language);
    story_text(language);
    name_choice(player_name, language);
    debut(language, &progress);

    return (0);
}